SET NAMES utf8;
SET time_zone = '+03:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `itmo`;
CREATE DATABASE `itmo` /*!40100 DEFAULT CHARACTER SET utf8 */;
DROP DATABASE IF EXISTS `itmo_test`;
CREATE DATABASE `itmo_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `itmo`;
