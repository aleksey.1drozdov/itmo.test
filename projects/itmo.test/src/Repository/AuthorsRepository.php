<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Author>
 *
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorsRepository extends ServiceEntityRepository
{
    const TABLE = 'authors';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    /**
     * @param Author $entity
     * @param bool $flush
     * @return Author
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveModel(Author $entity, bool $flush = true): Author
    {
        $this->_em->persist($entity);

        if ($flush) {
            $this->_em->flush();
        }

        return $entity;
    }

    /**
     * @param Author $authorModel
     * @param array $bookIds
     * @return Author
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\ORMException
     */
    public function attachAuthors(Author $authorModel, array $bookIds): Author
    {
        $authorModel->getBooks()->clear();

        if (! empty($bookIds)) {
            $data = [];
            foreach ($bookIds as $bookId) {
                $data[] = sprintf('(%d,%d)', $bookId, $authorModel->getId());
            }

            $sql = sprintf(
                'INSERT INTO books_authors (book_id, author_id) VALUES %s',
                implode(',', $data)
            );
            $this->_em->getConnection()->executeQuery($sql);
            $this->_em->refresh($authorModel);
        }

        return $authorModel;
    }

    /**
     * @param Author $model
     * @return bool
     * @throws \Throwable
     */
    public function delete(Author $model): bool
    {
        $this->_em->wrapInTransaction(function () use ($model) {
            $model->getBooks()->clear();
            $this->_em->remove($model);
        });

        return true;
    }
}
