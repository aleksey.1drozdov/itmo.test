<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use \Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BooksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @param Book $entity
     * @param bool $flush
     * @return Book
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveModel(Book $entity, bool $flush = true): Book
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }

        return $entity;
    }

    /**
     * @param Book $bookModel
     * @param array $authorIds
     * @return Book
     * @throws ORMException
     * @throws \Doctrine\DBAL\Exception
     */
    public function attachAuthors(Book $bookModel, array $authorIds): Book
    {
        $bookModel->getAuthors()->clear();

        if (! empty($authorIds)) {
            $data = [];
            foreach ($authorIds as $authorId) {
                $data[] = sprintf('(%d,%d)', $bookModel->getId(), $authorId);
            }

            $sql = sprintf(
                'INSERT INTO books_authors (book_id, author_id) VALUES %s',
                implode(',', $data)
            );
            $this->_em->getConnection()->executeQuery($sql);
            $this->_em->refresh($bookModel);
        }

        return $bookModel;
    }

    /**
     * @param Book $model
     * @return bool
     * @throws \Throwable
     */
    public function delete(Book $model): bool
    {
        $this->_em->wrapInTransaction(function () use ($model) {
            $model->getAuthors()->clear();
            $this->_em->remove($model);
        });

        return true;
    }

}
