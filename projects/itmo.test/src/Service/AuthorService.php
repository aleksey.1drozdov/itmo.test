<?php

namespace App\Service;

use App\Dto\UpsertAuthorDto;
use App\Entity\Author;
use App\Exceptions\NotFoundException;
use App\Repository\AuthorsRepository;

class AuthorService
{
    /**
     * @var AuthorsRepository
     */
    private $authorRepository;

    public function __construct(AuthorsRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @param int $id
     * @return Author
     * @throws NotFoundException
     */
    public function get(int $id): Author
    {
        $model = $this->authorRepository->find($id);

        if ($model === null) {
            throw new NotFoundException(Author::class, $id);
        }

        return $model;
    }

    /**
     * @param UpsertAuthorDto $dto
     * @return Author
     * @throws NotFoundException
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function upsert(UpsertAuthorDto $dto): Author
    {
        $model = ! empty($dto->id) ? $this->get($dto->id) : new Author();

        $model = $model
            ->setFirstname($dto->firstname)
            ->setLastname($dto->lastname)
            ->setPatronymic($dto->patronymic);

        $model = $this->authorRepository->saveModel($model);

        $model = $this->attachBooks($model, $dto->books ?: []);

        return $model;
    }

    /**
     * @param Author $authorModel
     * @param array $booksIds
     * @return Author
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\ORMException
     */
    public function attachBooks(Author $authorModel, array $booksIds): Author
    {
        return $this->authorRepository->attachAuthors($authorModel, $booksIds);
    }

    /**
     * @param int $authorId
     * @return array|mixed[]
     * @throws NotFoundException
     */
    public function getBooks(int $authorId)
    {
        return $this->get($authorId)->getBooks()->toArray();
    }

    /**
     * @param int $authorId
     * @return bool
     * @throws NotFoundException
     * @throws \Throwable
     */
    public function delete(int $authorId): bool
    {
        return $this->authorRepository->delete($this->get($authorId));
    }
}