<?php

namespace App\Service;

use App\Dto\UpsertBookDto;
use App\Entity\Book;
use App\Exceptions\NotFoundException;
use App\Repository\BooksRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BooksService
{
    const IMAGE_DIR = '/src/itmo.test/public/images/books/';
    const PUBLIC_PATH = '/images/books/';

    /**
     * @var BooksRepository
     */
    private $bookRepository;

    public function __construct(BooksRepository $booksRepository)
    {
        $this->bookRepository = $booksRepository;
    }

    /**
     * @param int $id
     * @return Book
     * @throws NotFoundException
     */
    public function get(int $id): Book
    {
        $model = $this->bookRepository->find($id);

        if ($model === null) {
            throw new NotFoundException(Book::class, $id);
        }

        return $model;
    }

    /**
     * @param UpsertBookDto $dto
     * @return Book
     * @throws NotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function upsert(UpsertBookDto $dto): Book
    {
        $model = ! empty($dto->id) ? $this->get($dto->id) : new Book();

        $model = $model
            ->setTitle($dto->title)
            ->setIsbn($dto->isbn)
            ->setYear($dto->year)
            ->setPages($dto->pages);

        $model = $this->bookRepository->saveModel($model);

        if ($dto->image !== null) {
            $model = $model->setImage($this->saveImage($model->getId(), $dto->image));
            $this->bookRepository->saveModel($model);
        }

        $model = $this->attachAuthor($model, $dto->authors ?: []);

        return $model;
    }

    /**
     * @param int $id
     * @param UploadedFile $file
     * @return string
     */
    private function saveImage(int $id, UploadedFile $file): string
    {
        $name = sprintf('%d.%s', $id, $file->getClientOriginalExtension());

        $file->move(self::IMAGE_DIR, $name);

        return self::PUBLIC_PATH . $name;
    }

    /**
     * @param Book $book
     * @param array $authorIds
     * @return Book
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\ORMException
     */
    public function attachAuthor(Book $book, array $authorIds): Book
    {
        return $this->bookRepository->attachAuthors($book, $authorIds);
    }

    /**
     * @param $bookId
     * @return \App\Entity\Author[]|\Doctrine\Common\Collections\ArrayCollection|\Doctrine\ORM\PersistentCollection
     * @throws NotFoundException
     */
    public function getAuthors($bookId)
    {
        return $this->get($bookId)->getAuthors()->toArray();
    }

    /**
     * @param int $bookId
     * @return bool
     * @throws NotFoundException
     * @throws \Throwable
     */
    public function delete(int $bookId): bool
    {
        return $this->bookRepository->delete($this->get($bookId));
    }
}