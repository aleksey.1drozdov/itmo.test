<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Book
 *
 * @ORM\Table(name="books", uniqueConstraints={@ORM\UniqueConstraint(name="title_isbn", columns={"title", "isbn"}), @ORM\UniqueConstraint(name="title_year", columns={"title", "year"})})
 * @ORM\Entity(repositoryClass="App\Repository\BooksRepository")
 */
class Book
{
    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"book"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=512, nullable=false)
     * @Groups({"book"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="isbn", type="string", length=13, nullable=false)
     * @Groups({"book"})
     */
    private $isbn;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     * @Groups({"book"})
     */
    private $year;

    /**
     * @var int
     *
     * @ORM\Column(name="pages", type="smallint", nullable=false, options={"unsigned"=true})
     * @Groups({"book"})
     */
    private $pages;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=13, nullable=false)
     * @Groups({"book"})
     */
    private $image;

    /**
     * @var PersistentCollection|Author[]
     *
     * @ORM\ManyToMany(targetEntity="Author")
     * @ORM\JoinTable(name="books_authors")
     */
    private $authors;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPages(): ?int
    {
        return $this->pages;
    }

    public function setPages(int $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    public function getAuthors()
    {
        return $this->authors;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $imageName): self
    {
        $this->image = $imageName;

        return $this;
    }
}
