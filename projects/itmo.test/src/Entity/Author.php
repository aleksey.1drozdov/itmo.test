<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Author
 *
 * @ORM\Table(name="authors", uniqueConstraints={@ORM\UniqueConstraint(name="lastname_firstname_patronymic", columns={"lastname", "firstname", "patronymic"})})
 * @ORM\Entity(repositoryClass="App\Repository\AuthorsRepository")
 */
class Author
{
    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"author"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
     * @Groups({"author"})
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
     * @Groups({"author"})
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="patronymic", type="string", length=255, nullable=false)
     * @Groups({"author"})
     */
    private $patronymic;

    /**
     * @var PersistentCollection|Book[]
     *
     * @ORM\ManyToMany(targetEntity="Book")
     * @ORM\JoinTable(name="books_authors")
     */
    private $books;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getBooks()
    {
        return $this->books;
    }
}
