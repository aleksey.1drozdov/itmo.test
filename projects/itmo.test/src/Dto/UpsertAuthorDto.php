<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UpsertAuthorDto extends AbstractDto
{
    /**
     * @Assert\Type(type = "integer")
     */
    public $id;

    /**
     * @Assert\NotNull()
     * @Assert\Type(type = "string")
     * @Assert\Length(min="2", max="255", allowEmptyString="false")
     */
    public $firstname;

    /**
     * @Assert\NotNull()
     * @Assert\Type(type = "string")
     * @Assert\Length(min="2", max="255", allowEmptyString="false")
     */
    public $lastname;

    /**
     * @Assert\NotNull()
     * @Assert\Type(type = "string")
     * @Assert\Length(min="2", max="255", allowEmptyString="false")
     */
    public $patronymic;

    /**
     * @Assert\All({
     *      @Assert\Type(type="integer")
     * })
     */
    public $books;
}