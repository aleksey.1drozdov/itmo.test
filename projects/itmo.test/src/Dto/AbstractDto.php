<?php

namespace App\Dto;

use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validation;

abstract class AbstractDto
{
    /**
     * @param array $fields
     */
    public function __construct(array $fields)
    {
        $this->fillFields($fields);
        $this->validateFields();
    }

    /**
     * @param array $fields
     * @return void
     */
    private function fillFields(array $fields): void
    {
        foreach ($fields as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * @return void
     */
    private function validateFields()
    {
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $violations = $validator->validate($this);

        if ($violations->count()) {
            throw new ValidatorException($violations);
        }
    }
}