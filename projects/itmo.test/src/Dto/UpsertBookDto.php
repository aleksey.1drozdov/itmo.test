<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UpsertBookDto extends AbstractDto
{
    /**
     * @Assert\Type(type = "integer")
     */
    public $id;

    /**
     * @Assert\NotNull()
     * @Assert\Type(type = "string")
     * @Assert\Length(min="2", max="255", allowEmptyString="false")
     */
    public $title;

    /**
     * @Assert\NotNull()
     * @Assert\Isbn(type = "isbn13")
     */
    public $isbn;

    /**
     * @Assert\NotNull()
     * @Assert\Regex(pattern="/^\d{4}$/"))
     */
    public $year;

    /**
     * @Assert\NotNull()
     * @Assert\Type(type="integer")
     */
    public $pages;

    /**
     * @Assert\All({
     *      @Assert\Type(type="integer")
     * })
     */
    public $authors;

    /**
     * @Assert\File(
     *     maxSize = "512k",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     mimeTypesMessage = "Please upload a valid image. PNG or JPEG. 512k"
     * )
     */
    public $image;
}