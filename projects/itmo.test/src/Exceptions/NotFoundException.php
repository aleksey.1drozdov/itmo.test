<?php

namespace App\Exceptions;

use Throwable;

class NotFoundException extends \Exception
{
    public function __construct(string $class, int $id, $code = 0, Throwable $previous = null)
    {
        $message = sprintf('%s.id#%d not found', $class, $id);

        parent::__construct($message, $code, $previous);
    }
}