<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends AbstractController
{
    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function api($data = [], int $status = 200, array $headers = [])
    {
        return JsonResponse::create(
            [
                'status' => 'ok',
                'data' => $data ?: []
            ],
            $status,
            $headers
        );
    }

    /**
     * @param \Throwable $exception
     * @param int $status
     * @return JsonResponse
     */
    protected function apiError(\Throwable $exception, int $status = 500)
    {
        if ($exception->getCode() > 0) {
            $status = $exception->getCode();
        }

        return JsonResponse::create(
            [
                'status' => 'error',
                'data' => [
                    'message' => $exception->getMessage()
                ]
            ],
            $status
        );
    }
}