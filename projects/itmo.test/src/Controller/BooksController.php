<?php

namespace App\Controller;

use App\Dto\UpsertBookDto;
use App\Service\BooksService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class BooksController extends BaseController
{
    private $booksService;
    private $serializer;

    public function __construct(BooksService $booksService, SerializerInterface $serializer)
    {
        $this->booksService = $booksService;
        $this->serializer = $serializer;
    }

    public function view(int $id)
    {
        try {
            return $this->api(
                $this->serializer->normalize($this->booksService->get($id), null, ['groups' => 'book'])
            );
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function create(Request $request)
    {
        try {
            $authors = !empty($request->get('authors')) ? $request->get('authors') : [];

            $this->booksService->upsert(new UpsertBookDto([
                'title' => $request->get('title'),
                'isbn' => $request->get('isbn'),
                'year' => (int) $request->get('year'),
                'pages' => (int) $request->get('pages'),
                'authors' => $authors,
                'image' => $request->files->get('image')
            ]));

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function update(int $id, Request $request)
    {
        try {
            $authors = !empty($request->get('authors')) ? $request->get('authors') : [];

            $this->booksService->upsert(new UpsertBookDto([
                'id' => $id,
                'title' => $request->get('title'),
                'isbn' => $request->get('isbn'),
                'year' => (int) $request->get('year'),
                'pages' => (int) $request->get('pages'),
                'authors' => $authors,
                'image' => $request->files->get('image')
            ]));

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function delete(int $id)
    {
        try {
            $this->booksService->delete($id);

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function getAuthors(int $id)
    {
        try {
            return $this->api(
                $this->serializer->normalize($this->booksService->getAuthors($id), null, ['groups' => 'author'])
            );
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }
}