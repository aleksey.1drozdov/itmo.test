<?php

namespace App\Controller;

use App\Dto\UpsertAuthorDto;
use App\Service\AuthorService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class AuthorsController extends BaseController
{

    private $authorsService;
    private $serializer;

    public function __construct(AuthorService $authorsService, SerializerInterface $serializer)
    {
        $this->authorsService = $authorsService;
        $this->serializer = $serializer;
    }

    public function view(int $id)
    {
        try {
            return $this->api(
                $this->serializer->normalize($this->authorsService->get($id), null, ['groups' => 'author'])
            );
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function create(Request $request)
    {
        try {
            $books = !empty($request->get('books')) ? $request->get('books') : [];

            $this->authorsService->upsert(new UpsertAuthorDto([
                'firstname' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'patronymic' => $request->get('patronymic'),
                'books' => $books
            ]));

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function update(int $id, Request $request)
    {
        $books = !empty($request->get('books')) ? $request->get('books') : [];

        try {
            $this->authorsService->upsert(new UpsertAuthorDto([
                'id' => $id,
                'firstname' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'patronymic' => $request->get('patronymic'),
                'books' => $books
            ]));

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function delete(int $id)
    {
        try {
            $this->authorsService->delete($id);

            return $this->api([], 204);
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }

    public function getAuthors(int $id)
    {
        try {
            return $this->api(
                $this->serializer->normalize($this->authorsService->getBooks($id), null, ['groups' => 'book'])
            );
        } catch (\Throwable $exception) {

            return $this->apiError($exception);
        }
    }
}