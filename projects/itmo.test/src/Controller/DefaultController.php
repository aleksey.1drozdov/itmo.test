<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('default.html.twig');
    }

    public function getSwaggerDoc()
    {
        $file = file_get_contents($this->getParameter('kernel.project_dir') . '/doc/api.yaml');

        return new Response($file);
    }
}