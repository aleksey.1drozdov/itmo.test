<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220702154458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $sql = <<<SQL

CREATE TABLE authors (
    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(255) NOT NULL, 
    lastname VARCHAR(255) NOT NULL, 
    patronymic VARCHAR(255) NOT NULL, 
    UNIQUE INDEX lastname_firstname_patronymic (lastname, firstname, patronymic), 
    PRIMARY KEY(id)) 
    DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;

CREATE TABLE `books` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `year` year(4) NOT NULL,
  `pages` smallint(5) unsigned NOT NULL,
  `image` varchar(255),
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_isbn` (`title`,`isbn`),
  UNIQUE KEY `title_year` (`title`,`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
CREATE TABLE `books_authors` (
  `book_id` bigint(20) unsigned NOT NULL,
  `author_id` bigint(20) unsigned NOT NULL,
  KEY `book_id` (`book_id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `books_authors_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  CONSTRAINT `books_authors_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE books_authors');
        $this->addSql('DROP TABLE authors');
        $this->addSql('DROP TABLE books');
    }
}
