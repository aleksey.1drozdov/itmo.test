<?php

namespace App\Tests\Dto;

use App\Dto\UpsertBookDto;
use App\Tests\TestCase;
use Faker\Factory;
use Symfony\Component\Validator\Exception\ValidatorException;

class CreateBookDtoTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @test
     */
    public function newInstanceTest($data, $error)
    {
        if ($error) {
            $this->expectException(ValidatorException::class);
        }

        $dto = new UpsertBookDto($data);

        $this->assertInstanceOf(UpsertBookDto::class, $dto);
    }

    public function dataProvider()
    {
        $faker = Factory::create();

        return [
            'accept' => [
                'data' => [
                    'title' => $faker->realText(255),
                    'isbn' => $faker->isbn13,
                    'year' => $faker->year,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => false
            ],
            'fail title 1' => [
                'data' => [
                    'title' => '1',
                    'isbn' => $faker->isbn13,
                    'year' => $faker->year,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => true
            ],
            'fail title 2' => [
                'data' => [
                    'title' => $faker->realText(300),
                    'isbn' => $faker->isbn13,
                    'year' => $faker->year,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => true
            ],
            'fail isbn' => [
                'data' => [
                    'title' => $faker->realText(255),
                    'isbn' => 123,
                    'year' => $faker->year,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => true
            ],
            'fail year 1' => [
                'data' => [
                    'title' => $faker->realText(255),
                    'isbn' => $faker->isbn13,
                    'year' => -2001,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => true
            ],
            'fail year 2' => [
                'data' => [
                    'title' => $faker->realText(255),
                    'isbn' => $faker->isbn13,
                    'year' => 938,
                    'pages' => $faker->numberBetween(1,32000)
                ],
                'error' => true
            ],
            'fail pages' => [
                'data' => [
                    'title' => $faker->realText(255),
                    'isbn' => $faker->isbn13,
                    'year' => 1000,
                    'pages' => '1'
                ],
                'error' => true
            ],
        ];
    }
}