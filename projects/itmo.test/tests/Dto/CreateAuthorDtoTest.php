<?php

namespace App\Tests\Dto;

use App\Dto\UpsertAuthorDto;
use App\Tests\TestCase;
use Faker\Factory;
use Symfony\Component\Validator\Exception\ValidatorException;

class CreateAuthorDtoTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @test
     */
    public function newInstanceTest($data, $error)
    {
        if ($error) {
            $this->expectException(ValidatorException::class);
        }

        $dto = new UpsertAuthorDto($data);

        $this->assertInstanceOf(UpsertAuthorDto::class, $dto);
    }

    /**
     * @return array[]
     */
    public function dataProvider()
    {
        $faker = Factory::create('ru_RU');

        return [
            'accept' => [
                'data' => [
                    'firstname' => $faker->firstName,
                    'lastname' =>  $faker->lastName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => false
            ],
            'fail firstname 1' => [
                'data' => [
                    'firstname' => $faker->realText(300),
                    'lastname' =>  $faker->lastName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail firstname 2' => [
                'data' => [
                    'lastname' =>  $faker->lastName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail firstname 3' => [
                'data' => [
                    'firstname' => null,
                    'lastname' =>  $faker->lastName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail lastname 1' => [
                'data' => [
                    'lastname' => $faker->realText(300),
                    'firstname' =>  $faker->firstName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail lastname 2' => [
                'data' => [
                    'firstname' =>  $faker->firstName,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail lastname 3' => [
                'data' => [
                    'firstname' =>  $faker->firstName,
                    'lastname' =>  null,
                    'patronymic' =>  $faker->userName
                ],
                'error' => true
            ],
            'fail patronymic 1' => [
                'data' => [
                    'lastname' => $faker->lastName,
                    'firstname' =>  $faker->firstName,
                    'patronymic' =>  $faker->realText(300)
                ],
                'error' => true
            ],
            'fail patronymic 2' => [
                'data' => [
                    'lastname' =>  $faker->lastName,
                    'firstname' =>  $faker->firstName,
                ],
                'error' => true
            ],
            'fail patronymic 3' => [
                'data' => [
                    'firstname' =>  $faker->firstName,
                    'lastname' =>  $faker->lastName,
                    'patronymic' =>  null
                ],
                'error' => true
            ],
        ];
    }
}