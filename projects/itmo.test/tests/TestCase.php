<?php

namespace App\Tests;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TestCase extends KernelTestCase
{
    /**
     * @return void
     */
    protected function tearDown(): void
    {
        $this->truncateEntities();
    }

    /**
     * @return mixed
     */
    public function getEntityManager()
    {
        return self::bootKernel()->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @return void
     */
    private function truncateEntities()
    {
        $purger = new ORMPurger($this->getEntityManager());
        $purger->purge();
    }
}