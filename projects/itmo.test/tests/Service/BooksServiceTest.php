<?php

namespace App\Tests\Service;

use App\Dto\UpsertAuthorDto;
use App\Dto\UpsertBookDto;
use App\Entity\Author;
use App\Entity\Book;
use App\Exceptions\NotFoundException;
use App\Service\AuthorService;
use App\Service\BooksService;
use App\Tests\TestCase;
use Faker\Factory;

class BooksServiceTest extends TestCase
{
    public $bookService;
    public $authorService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->authorService = new AuthorService(
            $this->getEntityManager()->getRepository(Author::class)
        );
        $this->bookService = new BooksService(
            $this->getEntityManager()->getRepository(Book::class)
        );
    }

    /** @test */
    public function getAccept()
    {
        $faker = Factory::create();

        $expected = $this->bookService->upsert(new UpsertBookDto(
            [
                'title' => $faker->realText(255),
                'isbn' => $faker->isbn13,
                'year' => $faker->year,
                'pages' => $faker->numberBetween(1,1000)
            ]
        ));

        $actual = $this->bookService->get($expected->getId());

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function getFail()
    {
        $this->expectException(NotFoundException::class);
        $this->bookService->get(-1);
    }

    /** @test  */
    public function upsertAcceptCreate()
    {
        $faker = Factory::create();

        $author1 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));
        $author2 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));

        $actual = $this->bookService->upsert(new UpsertBookDto(
            [
                'title' => $faker->realText(255),
                'isbn' => $faker->isbn13,
                'year' => $faker->year,
                'pages' => $faker->numberBetween(1,1000),
                'authors' => [$author1->getId(), $author2->getId()]
            ]
        ));
        $this->assertEquals(2, $actual->getAuthors()->count());
        $this->assertEquals($this->bookService->get($actual->getId()), $actual);
    }

    /** @test  */
    public function upsertAcceptUpdate()
    {
        $faker = Factory::create();

        $author1 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));
        $author2 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));

        $actual = $this->bookService->upsert(new UpsertBookDto(
            [
                'title' => $faker->realText(255),
                'isbn' => $faker->isbn13,
                'year' => $faker->year,
                'pages' => $faker->numberBetween(1,1000),
                'authors' => [$author1->getId(), $author2->getId()]
            ]
        ));

        $actual = $this->bookService->upsert(new UpsertBookDto(
            [   'id' => $actual->getId(),
                'title' => 'TEST',
                'isbn' => $actual->getIsbn(),
                'year' => $actual->getYear(),
                'pages' => $actual->getPages(),
                'authors' => [$author1->getId(), $author2->getId()]
            ]
        ));

        $this->assertEquals(2, $actual->getAuthors()->count());
        $this->assertEquals($this->bookService->get($actual->getId()), $actual);
    }

    /** @test  */
    public function delete()
    {
        $faker = Factory::create();

        $author1 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));
        $author2 = $this->authorService->upsert(new UpsertAuthorDto([
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'patronymic' => $faker->userName
        ]));

        $actual = $this->bookService->upsert(new UpsertBookDto(
            [
                'title' => $faker->realText(255),
                'isbn' => $faker->isbn13,
                'year' => $faker->year,
                'pages' => $faker->numberBetween(1,1000),
                'authors' => [$author1->getId(), $author2->getId()]
            ]
        ));

        $id = $actual->getId();

        $this->bookService->delete($actual->getId());

        $r = $this->getEntityManager()->getConnection()
            ->executeQuery('SELECT * FROM books_authors where book_id=?', [$id])
            ->fetch();

        $this->assertFalse($r);

        $this->expectException(NotFoundException::class);
        $this->bookService->get($id);
    }
}