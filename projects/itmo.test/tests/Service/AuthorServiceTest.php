<?php

namespace App\Tests\Service;

use App\Dto\UpsertAuthorDto;
use App\Dto\UpsertBookDto;
use App\Entity\Author;
use App\Entity\Book;
use App\Exceptions\NotFoundException;
use App\Service\AuthorService;
use App\Service\BooksService;
use App\Tests\TestCase;
use Faker\Factory;

class AuthorServiceTest extends TestCase
{
    public $authorsService;
    public $bookService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->bookService = new BooksService($this->getEntityManager()->getRepository(Book::class));
        $this->authorsService = new AuthorService($this->getEntityManager()->getRepository(Author::class));
    }

    /** @test */
    public function getFail()
    {
        $this->expectException(NotFoundException::class);
        $this->authorsService->get(-1);
    }

    /** @test */
    public function getAccept()
    {
        $faker = Factory::create();

        $expected = $this->authorsService->upsert(new UpsertAuthorDto(
            [
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'patronymic' => $faker->userName,
            ]
        ));

        $actual = $this->authorsService->get($expected->getId());

        $this->assertEquals($expected, $actual);
    }


    /** @test  */
    public function upsertAcceptCreate()
    {
        $faker = Factory::create();

        $book1 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->title,
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));
        $book2 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->title,
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));

        $actual = $this->authorsService->upsert(new UpsertAuthorDto(
            [
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'patronymic' => $faker->userName,
                'books' => [$book1->getId(), $book2->getId()],
            ]
        ));

        $this->assertEquals(2, $actual->getBooks()->count());

        $expected = $this->authorsService->get($actual->getId());
        $this->assertEquals(2, $expected->getBooks()->count());

        $this->assertEquals($expected, $actual);
    }

    /** @test  */
    public function upsertAcceptUpdate()
    {
        $faker = Factory::create();

        $book1 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->unique()->realText(255),
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));
        $book2 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->unique()->realText(255),
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));

        $actual = $this->authorsService->upsert(new UpsertAuthorDto(
            [
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'patronymic' => $faker->userName,
                'books' => [$book1->getId(), $book2->getId()],
            ]
        ));

        $actual = $this->authorsService->upsert(new UpsertAuthorDto(
            [
                'id' => $actual->getId(),
                'firstname' => 'TEST',
                'lastname' => $actual->getLastname(),
                'patronymic' => $actual->getPatronymic(),
                'books' => [$book1->getId(), $book2->getId()],
            ]
        ));

        $this->assertEquals(2, $actual->getBooks()->count());

        $expected = $this->authorsService->get($actual->getId());
        $this->assertEquals(2, $expected->getBooks()->count());

        $this->assertEquals($expected, $actual);
    }

    /** @test */
    public function delete()
    {
        $faker = Factory::create();

        $book1 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->unique()->realText(255),
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));
        $book2 = $this->bookService->upsert(new UpsertBookDto([
            'title' => $faker->unique()->realText(255),
            'isbn' => $faker->isbn13,
            'year' => $faker->year,
            'pages' => $faker->numberBetween(1,100),
        ]));

        $actual = $this->authorsService->upsert(new UpsertAuthorDto(
            [
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'patronymic' => $faker->userName,
                'books' => [$book1->getId(), $book2->getId()],
            ]
        ));

        $id = $actual->getId();

        $this->authorsService->delete($actual->getId());

        $r = $this->getEntityManager()->getConnection()
            ->executeQuery('SELECT * FROM books_authors where author_id=?', [$id])
            ->fetch();

        $this->assertFalse($r);

        $this->expectException(NotFoundException::class);
        $this->authorsService->get($id);
    }

}