#!/bin/bash

cd services

docker-compose up -d --build --force
cp ./dockers/php/.env ../projects/itmo.test/.env
docker exec -ti itmo.test_php bash -c 'chmod -R 775 /src/itmo.test'
docker exec -ti itmo.test_php bash -c 'chmod -R 777 /src/itmo.test/public/images/books'
docker exec -ti itmo.test_php bash -c 'chmod -R 777 /src/itmo.test/var/log'
docker exec -ti itmo.test_php bash -c 'composer install -d /src/itmo.test'
docker exec -ti itmo.test_php bash -c 'cd /src/itmo.test && php ./bin/console doctrine:migrations:migrate --env=test'
docker exec -ti itmo.test_php bash -c 'cd /src/itmo.test && php ./bin/console doctrine:migrations:migrate'
docker exec -ti itmo.test_php bash -c 'cd /src/itmo.test && php ./bin/phpunit'
